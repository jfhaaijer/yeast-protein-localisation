# Predicting Protein Localization in yeast with machine learning
![Version](https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000)
[![License: GPLv2](https://img.shields.io/badge/License-GPLv2-green.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)

> This study tries to find the best protein classifier from all the different classifiers the Weka Workbench has to offer.

## Abstract

There is a lot to be discovered about proteins. Bio-informatic localization predictors are great tools to elucidate protein function. These predictors use machine learning algorithms to classify a protein to their cell location. This research tried improve or find a better alternative to an earlier developed protein localization model and succeeded. The original model has an accuracy of 55 percent and the new alternative has an accuracy of almost 62%, this was achieved by using a Stacking algorithm and optimizing its hyper-parameters. The top two algorithms of those that were tested in this research are Stacking ans Sequential minimal optimization (SMO). The Stacking algorithm achieved an accuracy of 61.97% and the SMO algorithm has an accuracy of 60.30%. A Java application was created using this optimized Stacking model, it can classify the protein location of new yeast proteins if they have the correct attributes. However these predictions are only 60% accurate, in an ideal world the accuracy would be 100 percent. The SMO algorithm is a lot faster than the Stacking algorithm, for bigger data sets it would be more convenient to use the SMO algorithm. The Protein Location Predictor (Java Application) can be found here: https://bitbucket.org/jfhaaijer/javaproteinlocationpredictor/src/master/
 
## Contents
 
    1. Research paper
        The research paper is called: Protein_localisation_in_Yeast.pdf it was created from the Protein_localisation_in_Yeast.Rmd file. 
        
    2. The research log
        The research log is calles: Protein_localisation_in_Yeast_log.pdf, the code can be found in the Protein_localisation_in_Yeast_log.pdf file

    2. data
        This folder contains the different data files.

        -improved_yeast_data.arff = arff file used for Weka
    
        2.1 original
          This folder contains the original data from the uci machine learning repository (http://archive.ics.uci.edu/ml/datasets/yeast). These were used for the Exploratory Data Analysis in the research paper.

          -yeast.data = The data set.
          -yeast.names = Explanation of the attributes.


    3. info
        Contains three reseach papers about protein localization. 'A Probabilistic Classi cationSystem_for_Predicting_the_Cellular_Localization_Sites_of_Proteins_Horton_Nakai' is referenced in the research paper.

    4. weka
        Contains all the results from weka. For example buffers, configurations and classifier models.

        4.1 basic_algorithms
          Buffers of the results of SMO, ZeroR, SimpleLogistic, OneR, RandomForest, J48, NaiveBayes, BayesNet and IBk. 
          It also has a result buffer from the experimenter if you want to compare between these algorithms.

        4.2 meta_algorithms
          Meta algorithms configuration and results (meta-classifiers: Bagging, Boosting, Voting & Stacking)

        4.3 optimizing_top2
          Two folders with different configurations of the models (5 SMO & 3 Stacking). The best of each has a result buffer for more information. 

    5. license
        Contains the license of this research

## Install

Clone the repository.
```sh
git clone https://jfhaaijer@bitbucket.org/jfhaaijer/javaproteinlocationpredictor.git
```

## Support
for any problems or questions mail : j.f.haaijer@st.hanze.nl

## Author

 **Jildou Haaijer**

## License

This project is [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_